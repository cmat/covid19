﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Covid19.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CovidDataController : ControllerBase
    {
        private static readonly string BaseURL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/";
        
        [HttpGet]
        public async Task<IEnumerable<CovidData>> GetAsync()
        {
            HttpClient client = new HttpClient();

            var filedate = DateTime.Now.ToString("MM-dd-yyyy") + ".csv";

            var response = await client.GetAsync(BaseURL + filedate);

            var pageContents = await response.Content.ReadAsStringAsync();

            byte[] byteArray = Encoding.ASCII.GetBytes(pageContents.TrimEnd('\r', '\n'));
            MemoryStream stream = new MemoryStream(byteArray);

            using (var reader = new StreamReader(stream))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<CovidData>().ToArray();
                
                return records;               
            }
        }
    }
}
