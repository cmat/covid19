﻿using System;
using CsvHelper.Configuration.Attributes;

namespace Covid19
{
    public class CovidData
    {
        [Index(0)]
        public string Province { get; set; }
        [Index(1)]
        public string Country { get; set; }
        [Index(2)]
        public string LastUpdated { get; set; }
        [Index(3)]
        public int Confirmed { get; set; }
        [Index(4)]
        public int Deaths { get; set; }
        [Index(5)]
        public int Recovered { get; set; }
        [Index(6)]
        public double Latitude { get; set; }
        [Index(7)]
        public double Longitude { get; set; }
    }
}
